export const getLocal = () => {
    let data = localStorage.getItem('projects');
    // console.log(data);

    if(data){
        return JSON.parse(data);
    }
    return [];
}

export const setLocal = (payload) => {
    localStorage.setItem('projects', JSON.stringify(payload));
    // console.log(payload);
}

export const addData = (payload) => {
    let data = getLocal();
    // setLocal(data);
    
    if(data.length){
        let newData = [...data, payload]
        setLocal(newData);
    }else {
        setLocal([payload]);
    }
}

export const editData = (payload) => {
    let data = getLocal();

    let singleData = data.find( item => item.id === payload.id);

    if(singleData){
        data = data.filter( dt => dt.id !== singleData.id);
        data = [...data, payload];
        setLocal(data);
    }

}


export const deleteLocal = (id) => {
    let data = getLocal();
    // console.log(data);
    let singleData = data.find( item => item.id === id);

    if(singleData){
        data = data.filter( dt => dt.id !== singleData.id);
        setLocal(data);
    }

}